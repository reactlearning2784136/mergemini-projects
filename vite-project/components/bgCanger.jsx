import { useState } from 'react'
import './bgChanger.css'

function BgChanger() {
  
  const [color,setColor]=useState("olive");
  const [txtc,setTxtc]=useState("white");

  function handleClick(c){
    if(c==='yellow' || c==='pink' || c==='lavender' || c==='white'){
      setTxtc("black");
    }else{
      setTxtc("white");
    }
       setColor(c);
  }

  return (
    <>
    <div className='bg'
      style={{backgroundColor:color}}
    >
    <h1 className="text">About us</h1>
    <img src="/OIP.jpg" />
    <p className="text" style={{color:txtc}}> 
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis eligendi dolores voluptatum ad exercitationem rerum! Voluptatibus deleniti aliquid autem possimus explicabo saepe praesentium, quisquam hic odio, perspiciatis ipsa aspernatur delectus dolorum voluptatum atque veritatis assumenda sunt omnis qui. Amet, fugiat sapiente minus, cupiditate quos deleniti dolores esse deserunt voluptatibus at explicabo! Quos fugit vitae autem vero animi reprehenderit alias, aliquid saepe aspernatur illum, minima laboriosam, doloremque praesentium dignissimos? Excepturi voluptatibus quae nemo magnam possimus ducimus eius consectetur minus aliquid? Quis accusamus mollitia aspernatur. Eum incidunt quaerat harum facilis odio tenetur. Impedit hic accusantium sequi itaque. Voluptas consequuntur perferendis, beatae quae, consequatur quisquam inventore mollitia deleniti quia odit porro molestias aliquid aspernatur ullam atque deserunt amet? Exercitationem cupiditate quibusdam incidunt nemo assumenda, ducimus doloribus fugit sint beatae commodi accusamus tenetur dolores in. Quibusdam illo cum sed! Laborum repellat non minus beatae veritatis labore dolor possimus quis aliquam ipsum. Quibusdam labore similique doloribus a nihil. 

    </p>
    <div className='navbar'>
      <button className='pill' style={{backgroundColor:"red"}} onClick={()=>{handleClick("red")}}>RED</button>
      <button className='pill' style={{backgroundColor:"green"}} onClick={()=>{handleClick("green")}}>GREEN</button>
      <button className='pill' style={{backgroundColor:"blue"}} onClick={()=>{handleClick("blue")}}>BLUE</button>
      <button className='pill' style={{backgroundColor:"olive"}} onClick={()=>{handleClick("olive")}}>OLIVE</button>
      <button className='pill' style={{backgroundColor:"gray"}} onClick={()=>{handleClick("gray")}}>GRAY</button>
      <button className='pill txtBlack' style={{backgroundColor:"yellow"}} onClick={()=>{handleClick("yellow")}}>YELLOW</button>
      <button className='pill txtBlack' style={{backgroundColor:"pink"}} onClick={()=>{handleClick("pink")}}>PINK</button>
      <button className='pill' style={{backgroundColor:"purple"}} onClick={()=>{handleClick("purple")}}>PURPLE</button>
      <button className='pill txtBlack' style={{backgroundColor:"lavender"}} onClick={()=>{handleClick("lavender")}}>LAVENDER</button>
      <button className='pill txtBlack'style={{backgroundColor:"white"}} onClick={()=>{handleClick("white")}}>WHITE</button>
      <button className='pill' style={{backgroundColor:"black"}} onClick={()=>{handleClick("black")}}>BLACK</button>
    </div>
    </div>
  </>
  )
}

export default BgChanger
